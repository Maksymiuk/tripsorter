
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var ts = require('gulp-typescript');
var tsProject = ts.createProject('tsconfig.json');

gulp.task('dev', function () {
    return tsProject.src()
        .pipe(tsProject())
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./js/'));
});

gulp.task('prod', function () {
    return tsProject.src()
        .pipe(tsProject())
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./js/'));
});

gulp.task("default", ['prod'], function () {});