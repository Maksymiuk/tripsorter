Little app for recruiting task. App should display available connections and find faster/cheaper for selected cities.

Assumptions:
- I aimed at simplicity, mostly vanila technologies used
- no frameworks,
- project was based on HTML5 boilerplate
- Type Script
- from notable libraries, it uses select2 (select replacement based on jQuery, I do not use jQuery directly)
- algorithm used for pathfinding: Floyd Warshall Algorithm to find all indirect connections in directed graph, Dijkstra for finding lowest cost connection between two points in graph. Dijkstra is not the fastest possible algorithm, but is relativelly easy to implement.
- code compatible directly with most popular browsers, including IE11
- layout is responsive, although it is very simple, so it is not a big achievement