/**
 * Object containing all connection with cost and calculating
 * shortest connection using Dijkstra algorithm
 * @constructor
 */
class Paths{
    private connections: {[index: string]: {[index: string]: Formats.Deal}} = {};
    private connectionsCosts: {[index: string]: {[index: string]: number}} = {};
    private costs: {[index: string]: number};
    private parents: {[index: string]: string};

    /**
     * Find lowest cost node from current paths
     * @param {Array<string>} processed
     * @returns {string}
     */
    private lowestCostNode(processed: string[]): string{
        return Object.keys(this.costs).reduce((lowest: string, node: string): string => {
            if (lowest === null || this.costs[node] < this.costs[lowest]) {
                if (processed.indexOf(node) === -1) {
                    lowest = node;
                }
            }
            return lowest;
        }, null);
    };

    /**
     * Find possible paths
     * @param {string} start  Departure city
     * @param {string} stop  Destination city
     */
    private findPaths(start: string, stop: string): void{
        let cost: number;
        let children: {[index: string]: number};
        let newCost: number;
        let processed: string[] = [];
        let node: string;
        let tmp: {[index: string]: number} = {};

        // track lowest cost to reach each node
        tmp[stop] = Infinity;
        this.costs = (<any>Object).assign(tmp, this.connectionsCosts[start]);

        // track paths
        this.parents = {};
        this.parents[stop] = null;

        for (let child in this.connectionsCosts[start]) {
            this.parents[child] = start;
        }

        node = this.lowestCostNode(processed);

        while (node) {
            cost = this.costs[node];
            children = this.connectionsCosts[node];
            for (let n in children) {
                if (n !== start) {//prevent loops
                    newCost = cost + children[n];
                    if (!this.costs[n]) {
                        this.costs[n] = newCost;
                        this.parents[n] = node;
                    }
                    if (this.costs[n] > newCost) {
                        this.costs[n] = newCost;
                        this.parents[n] = node;
                    }
                }
            }
            processed.push(node);
            node = this.lowestCostNode(processed);
        }
    }

    /**
     * Find best path from all calculated
     * @param {string} stop Destination city
     * @returns {{distance: number, path: string[], details: Formats.Deal[]}}
     */
    private getOptimalPath(stop: string): Formats.Path{
        let optimalPath: string[] = [stop];
        let parent: string = this.parents[stop];
        let details: Formats.Deal[] = [];
        let i: number;
        let len: number;

        while (parent) {
            optimalPath.push(parent);
            parent = this.parents[parent];
        }
        optimalPath.reverse();

        for (i = 0, len = optimalPath.length; i < len - 1; i++){
            details.push(this.connections[optimalPath[i]][optimalPath[i+1]]);
        }

        return {
            distance: this.costs[stop],
            path: optimalPath,
            details: details
        };
    }

    /**
     * Main algorith funciton, uses Dijkstra algorithm to calculate shortest cost
     * from start to stop stations
     * @param {string} start  Departure station
     * @param {string} stop  Destination station
     * @returns {Formats.Path}
     */
    public getPath (start: string, stop: string): Formats.Path{
        this.findPaths(start, stop);
        return this.getOptimalPath(stop);
    };

    /**
     * Parse and populate internal data containers
     * @param {Formats.Data} data  connecitons data
     * @param {Function} costFn  cost calculating function
     */
    constructor (data: Formats.Data, costFn: (_: Formats.Deal) => number){
        let i: number;
        let departure: string;
        let arrival: string;
        let cost: number;

        //load connections
        i = data.deals.length;
        while (i--) {
            departure = data.deals[i].departure;
            arrival = data.deals[i].arrival;
            if (!this.connections[departure]) {
                this.connections[departure] = {};
                this.connectionsCosts[departure] = {};
            }

            cost = costFn(data.deals[i]);
            if (!this.connections[departure][arrival]
             || cost < this.connectionsCosts[departure][arrival]
            ){
                this.connections[departure][arrival] = data.deals[i];
                this.connectionsCosts[departure][arrival] = cost;
            }
        }
    }

}
