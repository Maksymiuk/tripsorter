/// <reference path="./interfaces/formats.ts" />

/**
 * Application controller
 * @created 31.01.2018
 * @author Rafał Maksymiuk <rafal.maksymiuk@gmail.com>
 */

(function() {

    const DOMForm = document.getElementById('search');
    let connectionsData : Formats.Data;

    /**
     * Load data from file
     * @param {Function} callback
     * @returns {Promise.<TResult>|*}
     */
    function loadData(callback: () => void) {
        let request = new XMLHttpRequest();
//        request.open('GET', '/data/response.json', true);
        request.open('GET', '/tripsorter/data/response.json', true);

        request.onload = (data) => {
            if (request.status >= 200 && request.status < 400) {
                connectionsData = JSON.parse(request.responseText);
                callback();
            }
        };
        request.onerror = function() {};
        request.send();
    }

    loadData( () => {
        Connections.init(connectionsData);
        PathsFinder.init(connectionsData);
        Template.populateFromSelect();
        DOMForm.addEventListener('submit', Template.doSearch.bind(this, connectionsData.currency));
    });

    Template.init();
})();
