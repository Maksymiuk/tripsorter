/**
 * Templating operations module
 */

namespace Template {
    const DOMForm: HTMLFormElement  = <HTMLFormElement> document.getElementById('search');
    const DOMFormFrom: HTMLSelectElement = <HTMLSelectElement> document.getElementById('from');
    const DOMFormTo: HTMLSelectElement = <HTMLSelectElement> document.getElementById('to');
    const DOMFormSubmit: HTMLButtonElement = <HTMLButtonElement> document.getElementById('searchButton');
    const DOMResult: HTMLDivElement = <HTMLDivElement> document.getElementById('result');
    let fromSelect: any;
    let toSelect: any;

    /**
     * Reset results
     */
    function reset(): void{
        document.body.classList.remove('state-result');
    }

    /**
     * Populate select with give  array of cities
     */
    export function populateFromSelect(): void {
        const stations = Connections.getStations().slice();
        let formatStations: object[] = [];

        stations.forEach( (el: string) => {
            formatStations.push({ value: el, label: el })
        });
        fromSelect.setChoices(formatStations, 'value', 'label', true);

    }

    /**
     * Populate destinations list depending on origin place
     * With example data all destination points are accesible from all origin points but it might not be the case all the time
     * @param {Event} event
     */
    function populateToSelect( event: Event ): void {
        const ev: HTMLOptionElement =  <HTMLOptionElement> event.target;
        const from: string = ev.value;
        const stations: string[] = Connections.getDestinations(from);
        let formatStations: object[] = [];

        if (stations) {
            toSelect.enable();
            stations.forEach( (el: string) => {
                formatStations.push({ value: el, label: el })
            });
            toSelect.setChoices(formatStations, 'value', 'label', true);
            toSelect.removeActiveItems();
            //     .trigger('change');
        }
        DOMFormSubmit.setAttribute('disabled', 'disabled');
    }

    /**
     * Event handler to attach to form submit performing path search
     * @param {string} currency corrency string
     * @param {Event} e
     */
    export function doSearch(currency: string, e: Event): void {
        const from: string = DOMFormFrom.value;
        const to: string = DOMFormTo.value;
        const typeElement: HTMLInputElement = DOMForm.querySelector('input[name=type]:checked');
        const type: string = typeElement.value;
        let res: Formats.Path;

        e.preventDefault();

        if (!from || !to) {
            return;
        }

        switch (type) {
            case
            'cheapest' :
                res = PathsFinder.getPathValue(from, to);
                break
            case
            'fastest' :
                res = PathsFinder.getPathTime(from, to);
                break
        }

        res.currency = currency;
        populateResult(res);
    }

    /**
     * Create results list html
     * @param {Formats.Path} result
     * @returns {string}
     */
    function populateListElements(result: Formats.Path): string{
        let discount: number;
        let html: string = '';

        result.details.forEach(function (el) {

            if (el.discount > 0) {
                discount = Math.round(el.cost * (100 - el.discount) / 100);
            }

            html +=
                `<li>
                    <span class="dot"><span class="in-dot"></span></span>
                    <img class="icon" src="/tripsorter/img/${el.transport}.png" alt="${el.transport}"/>
                    <div class="connection">${el.departure}<span class="arrow">&gt;</span> ${el.arrival}</div>
                    <div class="price-container">
                        <span class="price ${(discount ? 'discount' : '')}">${el.cost} ${result.currency}</span>
                        ${(discount ? `<span class="promo">${discount} ${result.currency}</span>` : '')}
                    </div>
                    <div>${el.transport} ${el.reference} for ${el.duration.h}h${el.duration.m}</div>
                </li>`;
        });
        return html;
    }

    /**
     * Populate container with path search result
     * @param {Formats.Path} result
     */
    function populateResult(result: Formats.Path): void {
        let html: string;
        let totalTime: number = 0;
        let totalCost: number = 0;
        const rows = populateListElements(result);

        result.details.forEach(function (el) {
            if (el.discount > 0) {
                totalCost += Math.round(el.cost * (100 - el.discount) / 100);
            } else {
                totalCost += el.cost;
            }
            totalTime += (parseInt(el.duration.h, 10) * 60) + parseInt(el.duration.m, 10);
        });

        html =
           `<h2> Route from <strong>${result.path[0]}</strong> to <strong>${result.path[result.path.length - 1]}</strong></h2>
            <ul> ${rows} </ul>
            <h3> Total: <span class="total-time"> ${Math.floor(totalTime / 60) }h${(totalTime % 60)}m </span><span class="total-cost">${totalCost} ${result.currency}</span></h3>
            <button class="reset">Reset</button>`;

        DOMResult.innerHTML = html;

        document.body.classList.add('state-result');
        document.querySelector('.reset').addEventListener('click', reset);
    }

    /**
     * Initialize module
     */
    export function init(): void {
        fromSelect = new Choices(DOMFormFrom, {
            noChoicesText:  'No cities loaded'
        });
        toSelect = new Choices(DOMFormTo, {
            noChoicesText:  'No cities loaded'
        }).disable();

        fromSelect.passedElement.addEventListener('change', populateToSelect);
        toSelect.passedElement.addEventListener('change', () => DOMFormSubmit.removeAttribute('disabled'));
    }

};
