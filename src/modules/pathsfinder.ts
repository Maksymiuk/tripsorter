/**
 * Module creating two paths finder objects with different cost methood
 * It will be cashed for future requests
 */
namespace PathsFinder {
    let data: Formats.Data;
    let pathValue: Paths;
    let pathTime: Paths;

    /**
     * Path cost calculating funciton using trip time
     * @param {Formats.Deal} el
     * @returns {number}
     */
    function costTime(el: Formats.Deal): number {
        return (parseInt(el.duration.h, 10) * 60) + parseInt(el.duration.m, 10);
    }

    /**
     * Path cost calculating function using trip cost
     * @param {Formats.Deal} el
     * @returns {number}
     */
    function costValue(el: Formats.Deal): number{
        return (el.cost * ((100 - el.discount) / 100 ));
    }

    /**
     * Get shortest path by time cost calculation
     * @param {string} from Departure city
     * @param {string} to Destination city
     * @returns {Formats.Path}
     */
    export function getPathTime(from: string, to: string): Formats.Path{
        if (!pathTime){
            pathTime = new Paths(data, costTime);
        }
        return pathTime.getPath(from, to);
    }

    /**
     * Get shortest path by distance
     * @param {string} from Departure city
     * @param {string} to Destination city
     * @returns {Formats.Path}
     */
    export function getPathValue(from: string, to: string): Formats.Path{
        if (!pathValue){
            pathValue = new Paths(data, costValue);
        }
        return pathValue.getPath(from, to);
    }

    /**
     * Init module by connecitons data
     * @param d {Formats.Data}
     */
    export function init(d: Formats.Data): void{
        data = d;
    }

};
