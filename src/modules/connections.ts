/**
 * Module containing all connections and generating possible connections between cities
 * @constructor
 */
namespace Connections {
    //stations dictionary
    let stations: string[] = [];
    //direct connections
    let connections: number[][] = [];
    //possible connections
    let allConnectionsMatrix: boolean[][] = [];

    /**
     * Add station to object dictionary
     * @param {string} station
     */
    function addStation(station: string): void {
        if (stations.indexOf(station) === -1) {
            stations.push(station);
        }
    }

    /**
     * Sort stations alphabetically
     */
    function sort(): void {
        stations = stations.sort();
    }

    /**
     * Add direct connection between stations
     * @param {String} departure station
     * @param {String} arrival station
     */
    function addConnection(departure: string, arrival: string): void {
        const departureId = stations.indexOf(departure);
        const arrivalId = stations.indexOf(arrival);

        if (departureId > -1 && arrivalId > -1) {
            if (!connections[departureId]) {
                connections[departureId] = [];
            }
            if (connections[departureId].indexOf(arrivalId) === -1) {
                connections[departureId].push(arrivalId);
            }
        }
    }

    /**
     * Calculate all possible connections between stations using
     * Floyd Warshall Algorithm to calculate transitive closure
     */
    function calculateAllConnections(): void {
        let i: number;
        let j: number;
        let k: number;
        const stationsLength = stations.length;

        //initialise connections matrix
        for (i = 0; i < stationsLength; i++) {
            allConnectionsMatrix[i] = [];
            for (j = 0; j < stationsLength; j++) {
                allConnectionsMatrix[i][j] = false;
            }
        }

        //copy original connections data
        for (i = 0; i < connections.length; i++) {
            for (j = 0; j < connections[i].length; j++) {
                allConnectionsMatrix[i][connections[i][j]] = true;
            }
        }

        for (k = 0; k < stationsLength; k++) {
            for (i = 0; i < stationsLength; i++) {
                for (j = 0; j < stationsLength; j++) {
                    allConnectionsMatrix[i][j] = allConnectionsMatrix[i][j] || (allConnectionsMatrix[i][k] && allConnectionsMatrix[k][j]);
                }
            }
        }
    }

    /**
     * Initialise object data containers
     * @param {Formats.Data} data
     */
    function loadData(data: Formats.Data) {
        let i: number;
        let deal = data.deals[i];

        //get stations
        i = data.deals.length;
        while (i--) {
            deal = data.deals[i];
            addStation(deal.departure);
            addStation(deal.arrival);
        }
        sort();

        //load connections
        i = data.deals.length;
        while (i--) {
            deal = data.deals[i];
            addConnection(deal.departure, deal.arrival);
        }
    }

    /**
     * Stations getter
     * @returns {Array}
     */
    export function getStations (): string[]{
        return stations;
    };

    /**
     * All possible destinations for given station
     * @param {String} fromStation
     * @returns {Array<String>}
     */
    export function getDestinations(fromStation: string): string[]{
        const fromId = stations.indexOf(fromStation);
        let destinations = stations
            .filter( (el, id) => {
                return ((allConnectionsMatrix[fromId][id] && fromId != id) ? el : false);
            });
        return destinations;
    }

    export function init(data: Formats.Data): void{
        loadData(data);
        calculateAllConnections();
    }

};
