/**
 *  common data types
 */
namespace Formats {
    /**
     * Duration of the trip
     */
    interface Duration{
        h: string,
        m: string
    }
    /**
     * One trip setting
     */
    export interface Deal{
        transport: string,
        departure: string,
        arrival: string,
        duration: Duration,
        cost: number,
        discount: number,
        reference: string
    }
    /**
     * Complete data of available connections
     */
    export interface Data{
        currency: string,
        deals: Deal[]
    }
    /**
     * Paths to represent result of trip finder algorithm
     */
    export interface Path{
        distance: number,
        path: string[],
        details: Deal[],
        currency?: string
    }
}